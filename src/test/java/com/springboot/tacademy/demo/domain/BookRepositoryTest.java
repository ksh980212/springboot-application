package com.springboot.tacademy.demo.domain;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookRepositoryTest {

    @Autowired
    BookRepository bookRepository;

    @After
    public void cleanup() {
        bookRepository.deleteAll();
    }

    @Test
    public void testSave() {
        Book book = new Book();
        book.setName("boot-spring-boot");
        book.setIsbn10("012345");
        book.setIsbn13("01234");

        assertThat(book.isNew()).isTrue();

        bookRepository.save(book);

        assertThat(book.isNew()).isFalse();
    }

    @Test
    public void testFindByNameLike() {

        Book book = new Book();
        book.setName("boot-spring");
        book.setIsbn10("012345");
        book.setIsbn13("01234");
        bookRepository.save(book);

         List<Book> books= bookRepository.findByNameLike("book");
         assertThat(books).isEmpty();

         books = bookRepository.findByNameLike("boot%");
         assertThat(books).isNotEmpty();
    }
}
