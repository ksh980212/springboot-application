package com.springboot.tacademy.demo.service;

import com.springboot.tacademy.demo.domain.Book;

import java.util.Optional;

public interface BookService {

    Optional<Book> findById(Long id);
}
